#!/bin/bash

HOME=/home/ubuntu

# export MYSQL_USER=root
# export MYSQL_PASSWORD=diman95crypto
# export MYSQL_ALGOSITE_DATABASE_NAME=algosite

export WORKON_HOME=$HOME/Envs
export PROJECT_HOME=$HOME/osr/devstack_docker

export SITE_HOME=$PROJECT_HOME/buildStack
export CONF_HOME=$PROJECT_HOME/conf

# export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'

# export ALGOSITE_STATIC=$HOME/quantforces/static_content/static
# export ALGOSITE_MEDIA=$HOME/quantforces/static_content/media

# export ALGOSITE_DEBUG=0

# export ALGOSITE_SANDBOX=$HOME/quantforces/static_content/quantbox

source /usr/local/bin/virtualenvwrapper.sh

cd /home/ubuntu/devstack
. openrc admin
workon buildstack
cd $SITE_HOME