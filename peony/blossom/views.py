from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

def main_page(request):
    if request.user.is_authenticated():
        return redirect("profile_main")
    else:
        return render(request, "index.html")

@login_required
def profile_page(request):
    context = {
        "active_page": "index",
        "instances": []
    }
    return render(request, "profile/index.html", context)
