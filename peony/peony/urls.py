from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('blossom.views',
    url(r'^$', 'main_page', name='index'),
    url(r'^accounts/profile/$', 'profile_page', name="profile_main"),
    url(r'^admin/', include(admin.site.urls)),
)
