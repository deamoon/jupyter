/**
* Simple API for buildstack
* Should be added error handler
*
* For Java   -> Maven
* For Go     -> Go SDK
* For Python -> cx_Freeze
* For c++    -> CMake
*
* This builders have some additional options, 
* so in future it can be successfully used.
* 
* Programm was tested with typical helloworld programms.
*/

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <iomanip>
#include <cstdlib>
#include <libconfig.h++>
#include <fstream>
#include <exception>

using namespace libconfig;
using namespace std;

/**
* Getting file extension
* @param FileName input filename
* @return File extension or nothing
*/
string GetFileExtension(const string& FileName)
{
  if(FileName.find_last_of(".") != string::npos) {
     return FileName.substr(FileName.find_last_of(".")+1);
  }
  return "";
}

/**
* Getting file name without extension
* @param FileName input filename
* @return Filename without extension or nothing
*/
string remove_extension(const string& filename) {
  size_t lastdot = filename.find_last_of(".");
  if (lastdot == string::npos) {
    return filename;
  }
  return filename.substr(0, lastdot); 
}

/**
* Compiling Python file
*
* Okay. This setup.py file has a lot of options,
* so they can be added in future for typing from keyboard or etc.
*
* @param FileName input filename
* @return Filename without extension
*/
string PythonComp (const string& filename)
{
  string file_without_ext = remove_extension(filename);

  ofstream outfile ("setup.py");
  outfile << "from cx_Freeze import setup, Executable" << endl << endl;
  outfile << "setup(" << endl ;
  outfile << "     name = \""+file_without_ext+"\"," << endl;
  outfile << "     version = \"0.1\"," << endl;
  outfile << "     description = \"Best project of the World!\"," << endl;
  outfile << "     executables = [Executable(\""+filename+"\")]" << endl;
  outfile << ")";
  outfile.close();

  system("python setup.py build");
  system("rm setup.py");
  return(file_without_ext);
}

/**
* Compiling C++ file
*
* CMake also has some options. 
*
* @param FileName input filename
* @return Filename without extension
*/
string CPlusComp (const string& filename)
{
  string file_without_ext = remove_extension(filename);

  ofstream outfile ("CMakeLists.txt");
  outfile << "add_executable("+file_without_ext+" "+filename+")" << endl;
  outfile.close();

  system ("cmake ./");
  system ("make");
  system("rm CMakeCache.txt cmake_install.cmake  CMakeLists.txt  Makefile");
  system("rm -R CMakeFiles");

  return(file_without_ext);
}

/**
* Compiling Go file
*
* Idk about this compiler. Project or many files?
*
* @param FileName input filename
* @return Filename without extension
*/
string GoComp (const string& filename)
{
  string file_without_ext = remove_extension(filename);

  system( ("go build "+filename).c_str() ); 

  return (file_without_ext);
}

/**
* Compiling Java file
*
* A lot and a lot of options.
* mvn archetype:generate has variable settings
* and some stuff in pom.xml file
*
* Created a typical cfg
*
* @param FileName input filename
* @return Filename without extension
*/
string JavaComp (const string& filename)
{
  // Create tree of project
  system( ("sudo mvn archetype:generate -DgroupId=simple -DartifactId="+filename+" -DarchetypeArtifactId=maven-archetype-quickstart -Dversion=1.0 -DinteractiveMode=false").c_str() );

 // Edit pom.xml
  system( ("sudo sed -i '/<\\/dependencies>/a  <build>\\n   <plugins>\\n    <plugin>\\n     <groupId>org.apache.maven.plugins<\\/groupId>\\n     <artifactId>maven-jar-plugin<\\/artifactId>\\n     <configuration>\\n      <archive>\\n       <manifest>\\n        <addClasspath>true<\\/addClasspath>\\n        <classpathPrefix>lib\\/<\\/classpathPrefix>\\n        <mainClass>"+filename+"<\\/mainClass>\\n       <\\/manifest>\\n      <\\/archive>\\n     <\\/configuration>\\n    <\\/plugin>\\n   <\\/plugins>\\n  <\\/build>' ./"+filename+"/pom.xml").c_str() );

  // Move all classes to proj folder
  system( ("sudo mv *.java ./"+filename+"/src/main/java/simple/").c_str() );
  
  // Compiling
  system( ("sudo mvn package -f "+filename+"/pom.xml").c_str() );
  system( ("sudo mvn compile -f "+filename+"/pom.xml").c_str() );

  // Move compiled proj and remove working directory
  system( ("sudo mv "+filename+"/target/"+filename+"-1.0.jar ./").c_str() );
  system( ("sudo rm -R "+filename+"/").c_str() );

  return(filename+"-1.0.jar");
}



int main(int argc, char *argv[])
{
  if (argc < 2) {
    fprintf(stderr,"Usage: ./do_build file_name\n");
    exit(1);
  }

  // Connect config file
  Config cfg;
  try
  {
    cfg.readFile("config.cfg");
  }
  catch(const FileIOException &fioex)
  {
    cout << "I/O error while reading file." << endl;
    exit(1);
  }
  catch(const ParseException &pex)
  {
    cout << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << std::endl;
    exit(1);
  }

  // Where is file with code?
  try
  {
    string get_source_from = cfg.lookup("get_source_from");
  }
  catch(const SettingNotFoundException &nfex)
  {
    cout  << "No 'get_source_from' setting in configuration file." << endl;
    exit(1);
  }

  // Where must be a result?
  try
  {
    string put_compiled_code = cfg.lookup("put_compiled_code");
  }
  catch(const SettingNotFoundException &nfex)
  {
    cout << "No 'put_compiled_code' setting in configuration file." << endl;
    exit(1);
  }

  // Get input/output variable
  string get_source_from = cfg.lookup("get_source_from");
  string put_compiled_code = cfg.lookup("put_compiled_code");

  // Names
  string result = ""; // For result name from functions. 
  string full_name = argv[1];
  string file_ext = GetFileExtension(argv[1]);
  string file_name = remove_extension(argv[1]);

  // Get current path
  char the_path[256];
  getcwd(the_path, 255);

  // Get file from directory
  if (get_source_from != "./") {
  system( ("mv " + get_source_from + full_name + " " + the_path + "/").c_str() );
  }

  // Exist or not
  ifstream istr;
  istr.open(argv[1]);
  if (istr.fail()) {
    printf("File not found!\n");
    exit(1);
  }

  // Switch a compiler
  if (file_ext=="cpp") {
    printf("Compiling C++...\n");
    result=CPlusComp(full_name);
  }
  else if (file_ext=="go") {
    printf("Compiling Go...\n");
    result= GoComp(full_name);
  }
  else if (file_ext=="py")
  {
    printf("Compiling Python...\n");
    result = PythonComp(full_name);
  }
  else if (file_ext=="java")
  {
    // Get all java classes if it need
    if (get_source_from != "./") {
      system( ("mv " + get_source_from + "*java " + the_path + "/").c_str() );
    }
    printf("Compiling Java...\n");
    result=JavaComp(file_name);
  }
  else{
    printf("Unknown format.\n");
    exit(1);
  }

  // Remove original files
  // Java has already moved his classes, so it no need for him
  if (file_ext != "java") {
    system( ("rm "+ full_name).c_str() );
    }   

  // Move compiled files/directory (for Python)
  if (put_compiled_code != "./") {
    if (file_ext!="py"){
      system( ("mv "+ result+ " "+ put_compiled_code).c_str() );
    } else {
      system( ("mv ./build "+ put_compiled_code).c_str() );
    }
  }

  printf("Finish!\n");
  // Move result
  if (put_compiled_code!="./") {
    printf("Your file is: %s in %s \n",result.c_str(), put_compiled_code.c_str());
  } else {
    printf("Your file is: %s and it's just here!\n",result.c_str());
  }

  // Tip for python
  if (file_ext=="py"){
    printf("As u can see, it in \"build\" folder.\n");
  }
}
