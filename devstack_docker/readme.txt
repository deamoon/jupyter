Настройка сети и аккаунта openstack в видео к trystack:
	1. Создать сеть из-под аккаунта demo
		name = internal, IP = 192.168.38.0/24, DNS = 8.8.8.8
	2. Создать роутер
		name = router1, Set gateway, Add interface
	3. Настроить default rules
		ICMP, -1, -1
		TCP, 22, 80
	4. Создать instance, floating ip

Не забывай, что если работаешь на машине в openstack нужно разрешать порты в manage rules

Проброс портов:
	sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 54010 -j DNAT --to-destination 172.24.4.7:20
	Это проброс SSH(22) адреса 172.24.4.7 на порт 54010 у интерфейса eth0

Docker:
	Находим хороший контейнер на https://registry.hub.docker.com
		Если devstack
			. openrc admin
		docker pull ipython/scipyserver
		docker save ipython/scipyserver | glance image-create --is-public=True --container-format=docker --disk-format=raw --name ipython/scipyserver

Решено:
	Проблема сейчас в том, что нельзя настроить run параметры у docker, потому что его запускает nova boot
	Теперь можно:
		nova boot --image scipyserver --flavor m1.tiny --meta ENV_PASSWORD=1 scipyserver_test

Если вдруг не работает сеть и интерфейс br-ex не имеет адреса типа 172.24.4.1 при команде ifconfig, делай так:
	sudo ip addr flush dev br-ex
    sudo ip addr add 172.24.4.1/24 dev br-ex
    sudo ip link set br-ex up
    sudo route add -net 10.254.1.0/24 gw 172.24.4.1

Server error: An origin_mismatch error has been receieved from Google Drive. This may be beacuse you are running IPython notebook on a URL other than 127.0.0.1 or a port that is not in the range 8888-8899. Please see https://github.com/jupyter/jupyter-drive/issues/21 for more details