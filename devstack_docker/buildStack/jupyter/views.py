from django.shortcuts import render
from instance.models import Instance, Image, PublicIp, DockerImage
import json
from registration.users import UserModel
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect

@login_required
def get_jupyter(request):
    meta = {
        "PASSWORD": request.POST['password'],
    }
    context = {
        'public_url': "",
        'error_message': "",
    }

    if len(Instance.objects.filter(user__id=request.user.id)) > 0:
        context["error_message"] = "Only one server"

    try:
        free_port = PublicIp.objects.filter(status='Free')[0]
    except:
        context["error_message"] = "No public urls"

    if not context["error_message"]:
        base_image = DockerImage.objects.get(name="ipython/scipyserver")
        image = Image(name="test", meta=json.dumps(meta), base_image=base_image, user=request.user)
        image.save()
        # try:
        instance = Instance(name="test", flavor="m1.small", public_ip=free_port, image=image, user=request.user)
        instance.save()
        context["public_url"] = free_port.public_url
        # except:
        #     context["error_message"] = "Server error"

    return render(request, 'profile/instance.html', context)

@login_required
def get_all_instances(request):
    context = {
        'instances': Instance.objects.filter(user__id=request.user.id),
        'ip_free': len(PublicIp.objects.filter(status='Free')),
        'active_page': 'index',
    }
    return render(request, 'profile/index.html', context)

@login_required
def delete_instance(request, instance_id):
    instance = get_object_or_404(Instance, pk=instance_id)
    if request.user.id == instance.user.id:
        instance.delete()
    return redirect("profile_main")

def jupyter_main(request):
    if request.user.is_authenticated():
        return redirect("profile_main")
    else:
        context = {
            'ip_free': len(PublicIp.objects.filter(status='Free')),
        }
        context['ip_all'] = context['ip_free'] + len(PublicIp.objects.filter(status='Active'))
        return render(request, 'index.html', context)

def faq(request):
    context = {
            'ip_free': len(PublicIp.objects.filter(status='Free')),
        }
    context['ip_all'] = context['ip_free'] + len(PublicIp.objects.filter(status='Active'))
    return render(request, 'faq.html', context)

def sandbox(request):
    context = {
        'active_page': 'sandbox',
    }
    return render(request, 'profile/sandbox.html', context)