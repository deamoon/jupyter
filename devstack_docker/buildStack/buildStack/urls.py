from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from rest_framework import routers, serializers, viewsets
from instance.models import Instance, PublicIp, DockerImage, Image

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'is_staff')

class PublicIpSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PublicIp
        fields = ('id', 'status', 'public_url', 'iptable_port')

class DockerImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DockerImage
        fields = ('id', 'status', 'name', 'meta', 'web_port')

class ImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Image
        base_image = serializers.PrimaryKeyRelatedField(read_only='True')
        user = serializers.PrimaryKeyRelatedField(read_only='True')
        fields = ('id', 'name', 'web_port', 'base_image', 'meta', 'user')

class InstanceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Instance
        public_ip = serializers.PrimaryKeyRelatedField(read_only='True')
        image = serializers.PrimaryKeyRelatedField(read_only='True')
        user = serializers.PrimaryKeyRelatedField(read_only='True')
        fields = ('id', 'name', 'public_ip', 'flavor', 'image', 'user')

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class PublicIpViewSet(viewsets.ModelViewSet):
    queryset = PublicIp.objects.all()
    serializer_class = PublicIpSerializer

class DockerImageViewSet(viewsets.ModelViewSet):
    queryset = DockerImage.objects.all()
    serializer_class = DockerImageSerializer

class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer

class InstanceViewSet(viewsets.ModelViewSet):
    queryset = Instance.objects.all()
    serializer_class = InstanceSerializer

router = routers.DefaultRouter()
router.register(r'publicIp', PublicIpViewSet)
router.register(r'dockerImage', DockerImageViewSet)
router.register(r'image', ImageViewSet)
router.register(r'instance', InstanceViewSet)
router.register(r'users', UserViewSet)

urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),

    url(r'^$', "jupyter.views.jupyter_main", name='index'),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),

    url(r'^run/instance/$', "instance.views.run_instance"),
    url(r'^run/jupyter/$', "jupyter.views.get_jupyter"),
    url(r'^accounts/profile/$', "jupyter.views.get_all_instances", name='profile_main'),
    url(r'^accounts/profile/sandbox$', "jupyter.views.sandbox", name='sandbox'),

    url(r'^delete/(?P<instance_id>[0-9]+)$', "jupyter.views.delete_instance"),

    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': 'index'}, name='logout'),

    url(r'^faq/$', "jupyter.views.faq", name='faq'),
]

