from django.db import models
import ssh
from django.conf import settings
import json
import pytz
import datetime

utc = pytz.UTC

class PublicIp(models.Model):
    """
    Floating IP
    http://login.miptcloud.com:54121 - 54015
    http://login.miptcloud.com:54066 - 54016
    http://login.miptcloud.com:54145 - 54017
    http://login.miptcloud.com:54054 - 54018
    http://login.miptcloud.com:54071 - 54019
    http://login.miptcloud.com:54146 - 54020
    http://login.miptcloud.com:54147 - 54021
    """
    STATUS_LIST = (
        ('Active', 'Active'),
        ('Free', 'Free'),
        ('Delete', 'Delete'),
        ('Error', 'Error'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Error")
    public_url = models.CharField(max_length=100, default="unknown_url")
    iptable_port = models.CharField(max_length=20, default="unknown_iptable_port")
    def __unicode__(self):
        return "PublicIp " + " ".join([self.status, self.public_url, self.iptable_port])

class DockerImage(models.Model):
    """
    Anything docker container, which can be docker pull
    DockerImages = {
        "notebook" : ("ipython/notebook", "8888", {"USE_HTTP":"1", "PASSWORD":kwargs.get("password", "admin")}),
        "wordpress" : ("tutum/wordpress", "80", {})
    }
    """
    STATUS_LIST = (
        ('Up', 'Up'),  # image was installed on server
        ('Error', 'Error'),
        ('Delete', 'Delete'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Error")
    name = models.CharField(max_length=200, unique=True, default="unknown_image") # tutum/wordpress
    meta = models.TextField(default="{}", blank=True)
    web_port = models.CharField(max_length=20, default="unknown_port")

    date_creation = models.DateTimeField(auto_now_add=True)
    date_last_modification = models.DateTimeField(auto_now=True)

    docker_id = models.CharField(max_length=200, default="unknown_instance_id")

    def save(self, *args, **kwargs):
        if self.meta:
            dict(json.loads(self.meta))  # check format meta
        else:
            self.meta = '{}'

        create = kwargs.get('not_create', True)
        if create and self.status != 'Up':
            self.__create()

        super(DockerImage, self).save(*args, **kwargs)

    def __create(self):
        try:
            _, res_save = ssh.docker_create(self)
            self.docker_id = res_save['id']
            self.status = 'Up'
            return res_save
        except:
            self.status = 'Error'
            raise

    def delete_nova(self):
        try:
            res_create = ssh.docker_delete(self)
            self.status = 'Delete'
            return res_create
        except:
            self.status = 'Error'
            raise

    def __unicode__(self):
        return "DockerImage " + " ".join([self.status, self.name])

class Image(models.Model):
    """
    Users docker container with one open web_port and meta information
    Example:
        meta = {"PASSWORD":admin}
        docker run -e PASSWORD=admin
    """
    name = models.CharField(max_length=200, default="unknown_image") # wordpress
    web_port = models.CharField(max_length=20, default="unknown_port", blank=True)

    base_image = models.ForeignKey(DockerImage)

    meta = models.TextField(default="{}", blank=True)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_last_modification = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def save(self, *args, **kwargs):
        if not self.meta:
            self.meta = '{}'
        parent_dict = dict(json.loads(self.base_image.meta))
        my_dict = dict(json.loads(self.meta))
        parent_dict.update(my_dict)
        self.meta = json.dumps(parent_dict)
        if self.base_image.web_port != "unknown_port" and self.base_image.web_port:
            self.web_port = self.base_image.web_port
        super(Image, self).save(*args, **kwargs)

    def __unicode__(self):
        return "Image " + " ".join([self.name, self.web_port, str(self.base_image), self.user.username])

class Instance(models.Model):
    STATUS_LIST = (
        ('Active', 'Active'), # when instance acces from port 54121
        ('Up', 'Up'),  # when instance is running but doesn't acces from port 54121
        ('Down', 'Down'), # when instance is not running but exists in openstack
        ('Error', 'Error'),
        ('Delete', 'Delete'),
    )
    FLAVOR_LIST = (
        ('m1.nano', 'm1.nano, 1 VCPU, 64 MB, 0 GB'),
        ('m1.micro', 'm1.micro, 1 VCPU, 128 MB, 0 GB'),
        ('m1.heat', 'm1.heat, 1 VCPU, 512 MB, 0 GB'),
        ('m1.tiny', 'm1.tiny, 1 VCPU, 512 MB, 1 GB'),
        ('m1.small', 'm1.small, 1 VCPU, 2048 MB, 20 GB'),
        ('m1.medium', 'm1.medium, 2 VCPU, 4096 MB, 40 GB'),
        ('m1.large', 'm1.large, 4 VCPU, 8192 MB, 80 GB'),
        ('m1.xlarge', 'm1.xlarge, 8 VCPU, 16384 MB, 160 GB'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Error")

    name = models.CharField(max_length=200, default="unknown_name")

    floating_ip = models.CharField(max_length=50, default="unknown_floating_ip")

    public_ip = models.ForeignKey(PublicIp)

    flavor = models.CharField(max_length=20, choices=FLAVOR_LIST, default="m1.tiny")

    instance_id = models.CharField(max_length=200, default="unknown_instance_id")

    image = models.ForeignKey(Image)

    network_id = "6e5c81ec-a993-4c0e-8a3f-5d9a43b5dd80" # internal

    end_date = models.DateTimeField()
    date_creation = models.DateTimeField(auto_now_add=True)
    date_last_modification = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def save(self, *args, **kwargs):
        create = kwargs.get('not_create', True)
        activate = kwargs.get('not_avtivate', True)
        if create and self.status not in ('Active', 'Up'):
            self.__create()
            self.end_date = utc.localize(datetime.datetime.now()) + datetime.timedelta(hours=8)
        if activate and self.status != 'Active':
            self.__activate()
        super(Instance, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        delete = kwargs.get('not_delete', True)
        deactivate = kwargs.get('not_deavtivate', True)
        if delete:
            self.__deactivate()
        if deactivate:
            self.__delete_instance()
        super(Instance, self).delete(*args, **kwargs)

    def __delete_instance(self):
        ssh.nova_delete(self)

    def __create(self, **kwargs):
        try:
            instance_dict = ssh.create_instance(self)
            self.status = "Up"

            self.instance_id = instance_dict.get("id", "unknown_instance_id")
            self.floating_ip = instance_dict.get("ip",  "unknown_ip")
            return instance_dict
        except:
            self.status = "Error"
            raise

    def __deactivate(self):
        try:
            print(self.public_ip.status)
            if self.public_ip and self.public_ip.status == 'Active':
                ssh.deactivate(self.floating_ip, self.image.web_port, self.public_ip.iptable_port)
            self.public_ip.status = 'Free'
        except:
            self.public_ip.status = 'Error'
            raise
        finally:
            self.public_ip.save()

    def __activate(self):
        try:
            res_deactivate = self.__deactivate()
            res_activate = ssh.activate(self.floating_ip, self.image.web_port, self.public_ip.iptable_port)
            self.status = "Active"
            self.public_ip.status = 'Active'
            return str(res_deactivate) + str(res_activate)
        except:
            self.status = "Error"
            raise
        finally:
            self.public_ip.save()

    def __unicode__(self):
        return "Instance " + " ".join([self.name, str(self.image), str(self.floating_ip), self.status])