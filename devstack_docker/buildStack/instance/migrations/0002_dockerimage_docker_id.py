# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instance', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='dockerimage',
            name='docker_id',
            field=models.CharField(default=b'unknown_instance_id', max_length=200),
        ),
    ]
