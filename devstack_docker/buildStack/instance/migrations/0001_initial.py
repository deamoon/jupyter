# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DockerImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'Error', max_length=20, choices=[(b'Up', b'Up'), (b'Error', b'Error'), (b'Delete', b'Delete')])),
                ('name', models.CharField(default=b'unknown_image', unique=True, max_length=200)),
                ('meta', models.TextField(default=b'{}', blank=True)),
                ('web_port', models.CharField(default=b'unknown_port', max_length=20)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_last_modification', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'unknown_image', max_length=200)),
                ('web_port', models.CharField(default=b'unknown_port', max_length=20, blank=True)),
                ('meta', models.TextField(default=b'{}', blank=True)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_last_modification', models.DateTimeField(auto_now=True)),
                ('base_image', models.ForeignKey(to='instance.DockerImage')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Instance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'Error', max_length=20, choices=[(b'Active', b'Active'), (b'Up', b'Up'), (b'Down', b'Down'), (b'Error', b'Error'), (b'Delete', b'Delete')])),
                ('name', models.CharField(default=b'unknown_name', max_length=200)),
                ('floating_ip', models.CharField(default=b'unknown_floating_ip', max_length=50)),
                ('flavor', models.CharField(default=b'm1.tiny', max_length=20, choices=[(b'm1.nano', b'm1.nano, 1 VCPU, 64 MB, 0 GB'), (b'm1.micro', b'm1.micro, 1 VCPU, 128 MB, 0 GB'), (b'm1.heat', b'm1.heat, 1 VCPU, 512 MB, 0 GB'), (b'm1.tiny', b'm1.tiny, 1 VCPU, 512 MB, 1 GB'), (b'm1.small', b'm1.small, 1 VCPU, 2048 MB, 20 GB'), (b'm1.medium', b'm1.medium, 2 VCPU, 4096 MB, 40 GB'), (b'm1.large', b'm1.large, 4 VCPU, 8192 MB, 80 GB'), (b'm1.xlarge', b'm1.xlarge, 8 VCPU, 16384 MB, 160 GB')])),
                ('instance_id', models.CharField(default=b'unknown_instance_id', max_length=200)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_last_modification', models.DateTimeField(auto_now=True)),
                ('image', models.ForeignKey(to='instance.Image')),
            ],
        ),
        migrations.CreateModel(
            name='PublicIp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'Error', max_length=20, choices=[(b'Active', b'Active'), (b'Free', b'Free'), (b'Delete', b'Delete'), (b'Error', b'Error')])),
                ('public_url', models.CharField(default=b'unknown_url', max_length=100)),
                ('iptable_port', models.CharField(default=b'unknown_iptable_port', max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='instance',
            name='public_ip',
            field=models.ForeignKey(to='instance.PublicIp'),
        ),
        migrations.AddField(
            model_name='instance',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
