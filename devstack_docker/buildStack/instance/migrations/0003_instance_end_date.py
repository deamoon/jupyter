# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instance', '0002_dockerimage_docker_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='instance',
            name='end_date',
            field=models.DateTimeField(default="1995-02-04 15:12"),
            preserve_default=False,
        ),
    ]
