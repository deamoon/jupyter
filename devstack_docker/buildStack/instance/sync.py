from models import Instance, DockerImage, Image

def get_parsed_route_list(string):
    def _print_res(main_obj):
        for obj in main_obj:
            if isinstance(obj, dict) or isinstance(obj, list):
                _print_res(obj)
            print(obj)
        pass

    result_mass = string.split("Chain ")[1::]
    result = {}
    for part in result_mass:
        part = part.strip()
        line_count = 0
        chain_model = {}
        headers = []
        models = []
        for line in part.splitlines():
            if line_count is 0:
                result[line.split(" ")[0]] = {'models': models}
            elif line_count is 1:  # headers
                headers = line.split()
            else:
                model_obj = {}
                not_parsed_obj = line.split()
                for i in range(len(headers)):
                    model_obj[headers[i]] = not_parsed_obj[i]
                    pass

                if len(headers) < len(not_parsed_obj) and not (model_obj['target'] in 'DOCKER'):
                    add_array = not_parsed_obj[len(headers)::]
                    additional_info = {
                        'address_type': add_array[0],
                        'dist_port': add_array[1].split(':')[1],
                        'to_address': add_array[2].split(':')[1],
                        'to_port': add_array[2].split(':')[2]
                    }
                    model_obj['additional-info'] = additional_info
                    pass
                models.append(model_obj)
                pass

            line_count += 1
            pass
        if len(models) > 0:
            chain_model["sub_nodes"] = models
            pass
        pass
    return result


def main():
    string = """Chain PREROUTING (policy ACCEPT 9 packets, 540 bytes)
 pkts bytes target     prot opt in     out     source               destination
 3107  334K neutron-openvswi-PREROUTING  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 3107  334K nova-api-PREROUTING  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 2637  225K DOCKER     all  --  *      *       0.0.0.0/0            0.0.0.0/0            ADDRTYPE match dst-type LOCAL
    0     0 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:23 to:172.24.4.6:80
    0     0 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:24 to:10.254.1.2:8888
  786 47196 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:22 to:192.168.203.10:80

Chain INPUT (policy ACCEPT 21 packets, 1260 bytes)
 pkts bytes target     prot opt in     out     source               destination

Chain OUTPUT (policy ACCEPT 1544 packets, 93954 bytes)
 pkts bytes target     prot opt in     out     source               destination
 126K 7580K neutron-openvswi-OUTPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 126K 7582K nova-api-OUTPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 114K 6838K DOCKER     all  --  *      *       0.0.0.0/0           !127.0.0.0/8          ADDRTYPE match dst-type LOCAL
    4   240 DNAT       tcp  --  *      *       0.0.0.0/0            192.168.203.10       tcp dpt:54107 to:172.24.4.6:80
    2   120 DNAT       tcp  --  *      *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54011 to:172.24.4.6:80
    1    60 DNAT       tcp  --  *      *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54050 to:172.24.4.3:8888

Chain POSTROUTING (policy ACCEPT 1544 packets, 93954 bytes)
 pkts bytes target     prot opt in     out     source               destination
 127K 7715K neutron-openvswi-POSTROUTING  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 127K 7715K neutron-postrouting-bottom  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 127K 7718K nova-api-POSTROUTING  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 127K 7718K nova-postrouting-bottom  all  --  *      *       0.0.0.0/0            0.0.0.0/0
    0     0 MASQUERADE  all  --  *      !docker0  172.17.0.0/16        0.0.0.0/0

Chain DOCKER (2 references)
 pkts bytes target     prot opt in     out     source               destination

Chain neutron-openvswi-OUTPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain neutron-openvswi-POSTROUTING (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain neutron-openvswi-PREROUTING (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain neutron-openvswi-float-snat (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain neutron-openvswi-snat (1 references)
 pkts bytes target     prot opt in     out     source               destination
 127K 7715K neutron-openvswi-float-snat  all  --  *      *       0.0.0.0/0            0.0.0.0/0

Chain neutron-postrouting-bottom (1 references)
 pkts bytes target     prot opt in     out     source               destination
 127K 7715K neutron-openvswi-snat  all  --  *      *       0.0.0.0/0            0.0.0.0/0

Chain nova-api-OUTPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain nova-api-POSTROUTING (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain nova-api-PREROUTING (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain nova-api-float-snat (1 references)
 pkts bytes target     prot opt in     out     source               destination

Chain nova-api-snat (1 references)
 pkts bytes target     prot opt in     out     source               destination
 127K 7718K nova-api-float-snat  all  --  *      *       0.0.0.0/0            0.0.0.0/0

Chain nova-postrouting-bottom (1 references)
 pkts bytes target     prot opt in     out     source               destination
 127K 7718K nova-api-snat  all  --  *      *       0.0.0.0/0            0.0.0.0/0"""
    objc = get_parsed_route_list(string)
    print(objc)
    pass


if __name__ == '__main__':
	main()
