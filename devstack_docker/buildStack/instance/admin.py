from django.contrib import admin
from models import Instance, Image, DockerImage, PublicIp

admin.site.register(Instance)
admin.site.register(Image)
admin.site.register(DockerImage)
admin.site.register(PublicIp)