from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from models import Instance
from django.http import HttpResponse
import json
import ssh

from registration.users import UserModel
from instance.models import DockerImage, Image


def run_instance(request):
    docker_data = json.loads(request.body)

    if "docker_url" in docker_data:
        """ quay.io """
        repo_name = docker_data["docker_url"]
        owner = docker_data["namespace"]
    else:
        """ regisrtry hub """
        repo_name = docker_data["repository"]["repo_name"]
        owner = docker_data["repository"]["owner"]

    # print(repo_name)
    # user = UserModel().objects.get(username=owner)  # TODO, rewrite on docker_username

    try:
        dockerImage = DockerImage.objects.filter(name=repo_name)[0]
        dockerImage.delete_nova()
    except:
        dockerImage = DockerImage(name=repo_name)

    dockerImage.status = 'Uploading'
    dockerImage.save()

    # image = Image(name=repo_name+"_auto", base_image=DockerImage, user=user)

    return HttpResponse("OK", content_type="text/plain")