    1  sudo nano /etc/ssh/sshd_config
    2  sudo service ssh restart
    3  exit
    4  sudo apt-get update
    5  sudo apt-get -y install git git-review python-pip python-dev
    6  sudo apt-get -y upgrade
    7  wget -qO- https://get.docker.com/ | sh
    8  sudo usermod -aG docker ubuntu
    9  sudo nano /etc/default/docker
   10  sudo restart docker
   11  sudo nano /etc/hosts
   12  git clone https://github.com/stackforge/nova-docker
   13  cd nova-docker
   14  git checkout stable/juno
   15  git fetch https://review.openstack.org/stackforge/nova-docker refs/changes/83/117583/2 && git checkout FETCH_HEAD
   16  sudo python setup.py install
   17  cd ..
   18  git clone https://github.com/openstack-dev/devstack.git -b stable/juno
   19  cd devstack/
   20  nano local.conf
   21  ./stack.sh
   22  Horizon is now available at http://192.168.203.10/
   23  Keystone is serving at http://192.168.203.10:5000/v2.0/
   24  Examples on using novaclient command line is in exercise.sh
   25  The default users are: admin and demo
   26  The password: 1
   27  This is your host ip: 192.168.203.10
   28  ifconfig
   29  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   30  cd ..
   31  sudo cp nova-docker/etc/nova/rootwrap.d/docker.filters /etc/nova/rootwrap.d/
   32  cd devstack/
   33  . openrc admin
   34  docker pull ipython/notebook
   35  docker save ipython/notebook | glance image-create --is-public=True --container-format=docker --disk-format=raw --name ipython/notebook
   36  nova boot --image ipython/notebook --flavor m1.tiny  --meta ENV_PASSWORD=admin --meta ENV_USE_HTTP=1 notebook
   37  ifconfig
   38  10.254.1.2
   39  ping 10.254.1.2
   40  ping 10.254.1.2:8888
   41  wget 10.254.1.2:8888
   42  cat index.html
   43  ls
   44  rm index.html
   45  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   46  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   47  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   48  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   49  iptables -vnL --line-numbers
   50  sudo iptables -vnL --line-numbers
   51  iptables -vnL --line-numbers | grep 22
   52  sudo iptables -vnL --line-numbers | grep 22
   53  sudo iptables -vnL --line-numbers | grep 80
   54  sudo iptables -vnL --line-numbers | grep 0
   55  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   56  sudo iptables -vnL --line-numbers | grep 80
   57  sudo iptables -vnL --line-numbers
   58  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   59  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   60  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   61  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   62  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   63  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   64  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   65  ping 10.254.1.2:8888
   66  wget 10.254.1.2:8888
   67  cat index.html
   68  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   69  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   70  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   71  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   72  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   73  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   74  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   75  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   76  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   77  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   78  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   79  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 10.254.1.2:8888
   80  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   81  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   82  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
   83  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
   84  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   85  rm index.html
   86  wget 172.24.4.3:8888
   87  cat index.html
   88  rm index.html
   89  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   90  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
   91  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
   92  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
   93  wget 172.24.4.5:8888
   94  wget 192.168.38.2:8888
   95  ping 172.24.4.5
   96  ping 172.24.4.5:8888
   97  wget 172.24.4.5:8888
   98  wget 172.24.4.3:8888
   99  wget 172.24.4.5:8888
  100  ifconfig
  101  nova boot --image ipython/notebook --flavor m1.tiny  --nic net-id=47a43a7e-0b95-4407-88d4-048cadaba8a1 --meta ENV_PASSWORD=admin --meta ENV_USE_HTTP=1 notebook3
  102  wget 172.24.4.5:8888
  103  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  104  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.5:8888
  105  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.5:8888
  106  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  107  docker pull tutum/wordpress
  108  docker save tutum/wordpress | glance image-create --is-public=True --container-format=docker --disk-format=raw --name tutum/wordpress
  109  wget 172.24.4.6
  110  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  111  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.6:80
  112  sudo iptables -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.6:80
  113  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.6:80
  114  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  115  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 23 -j DNAT --to-destination 172.24.4.6:80
  116  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 23 -j DNAT --to-destination 172.24.4.6:80
  117  wget 172.24.4.6:80
  118  wget 192.168.38.4:8888
  119  wget 172.24.4.5:8888
  120  wget 10.254.1.2:8888
  121  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 24 -j DNAT --to-destination 10.254.1.2:8888
  122  wget 10.77.64.120:24
  123  wget 10.77.64.120:22
  124  cat index.html.4
  125  wget 10.77.64.120:23
  126  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 54005 -j DNAT --to-destination 10.254.1.2:8888
  127  wget 10.77.64.120:54005
  128  wget 54005
  129  wget 10.254.1.2:8888
  130  ifconfig
  131  wget 192.168.203.10:54005
  132  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 54006 -j DNAT --to-destination 172.24.4.6:80
  133  wget 192.168.203.10:54006
  134  netstat
  135  netstat | grep 54006
  136  netstat | grep 54005
  137  wget 192.168.203.10:54006
  138  wget 172.24.4.6:80
  139  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 54107 -j DNAT --to-destination 172.24.4.6:80
  140  wget eth0:54107
  141  wget 192.168.203.10:54107
  142  sudo iptables -t nat -A OUTPUT -p tcp --dport 54107 -d 192.168.203.10 -j DNAT --to-destination 172.24.4.6:80
  143  wget 192.168.203.10:54107
  144  get 192.168.203.10:54107
  145  sudo iptables -t nat -A OUTPUT -p tcp --dport 22 -d 192.168.203.10 -j DNAT --to-destination 172.24.4.6:80
  146  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  147  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.6:80
  148  wget 192.168.203.10:54107
  149  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.6:80
  150  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:54107
  151  sudo iptables -t nat -A OUTPUT -p tcp --dport 22 -d 192.168.203.10 -j DNAT --to-destination 192.168.203.10:54107
  152  wget 192.168.203.10:22
  153  sudo iptables -t nat -D OUTPUT -p tcp --dport 22 -d 192.168.203.10 -j DNAT --to-destination 192.168.203.10:54107
  154  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:54107
  155  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.6:80
  156  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  157  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  158  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:54107
  159  wget 192.168.203.10:54107
  160  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  161  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:54107
  162  django-admin.py startproject test1
  163  cd test1/
  164  python manage.py migrate
  165  ls
  166  python manage.py migrate
  167  python manage.py
  168  python manage.py syncdb
  169  python manage.py runserver 192.168.203.10:8889
  170  cat /etc/ssh/sshd_config
  171  sudo service ssh restart
  172  sudo passwd
  173  passwd
  174  sudo -i
  175  ls
  176  sudo /sbin/iptables -I INPUT -p tcp -m tcp --dport 54107 -j ACCEPT
  177  iptables -nvL
  178  sudo iptables -nvL
  179  wget 172.24.4.6:80
  180  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 54010 -j DNAT --to-destination 172.24.4.6:80
  181  wget 192.168.203.10:54010
  182  sudo iptables -nvL | grep 54010
  183  iptables -A INPUT -i lo -j ACCEPT
  184  sudo iptables -A INPUT -i lo -j ACCEPT
  185  wget 192.168.203.10:54010
  186  netstat -tulpn | grep :54010
  187  sudo netstat -tulpn | grep :54010
  188  iptables -t nat -A PREROUTING -p tcp --dport 54011 -j DNAT --to 172.24.4.6:80
  189  sudo iptables -t nat -A PREROUTING -p tcp --dport 54011 -j DNAT --to 172.24.4.6:80
  190  wget 192.168.203.10:54011
  191  sudo iptables -t nat -A OUTPUT -p tcp --dport 54011 -j DNAT --to 172.24.4.6:80
  192  wget 192.168.203.10:54011
  193  iptables -A INPUT -i eth0 -j ACCEPT
  194  sudo iptables -A INPUT -i eth0 -j ACCEPT
  195  wget 192.168.203.10:54011
  196  iptables -L -nv -t nat
  197  sud oiptables -L -nv -t nat
  198  sudo iptables -L -nv -t nat
  199  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  200  sudo iptables -L -nv -t nat
  201  sudo iptables -t nat -D OUTPUT -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  202  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:54107
  203  sudo iptables -L -nv -t nat
  204  wget 192.168.203.10:54107
  205  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:54107
  206  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  207  sudo iptables -L -nv -t nat
  208  sudo iptables -L -nv
  209  wget 172.24.4.7
  210  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  211  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.7:80
  212  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.7:80
  213  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  214  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  215  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.7:80
  216  sudo iptables -L -nv
  217  sudo iptables -L -nv -t nat
  218  cd ..
  219  . openrc demo
  220  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.7:80
  221  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  222  nova boot --image ipython/notebook --flavor m1.tiny  --nic net-id=47a43a7e-0b95-4407-88d4-048cadaba8a1 --meta ENV_PASSWORD=admin --meta ENV_USE_HTTP=1 notebook4
  223  wget 172.24.4.3:8888
  224  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  225  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  226  sudo iptables -L -nv -t nat
  227  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  228  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  229  sudo iptables -t nat -D OUTPUT -p tcp --dport 22 -d 192.168.203.10 -j DNAT --to-destination 172.24.4.6:80
  230  sudo iptables -t nat -A OUTPUT -p tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  231  sudo iptables -t nat -D OUTPUT -p tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  232  sudo iptables -t nat -A OUTPUT -p tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  233  sudo iptables -t nat -D OUTPUT -p tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  234  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  235  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  236  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  237  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  238  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  239  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  240  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 54050 -j DNAT --to-destination 172.24.4.3:8888
  241  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 54050 -j DNAT --to-destination 172.24.4.3:8888
  242  sudo iptables -t nat -A OUTPUT -p tcp --dport 54050 -j DNAT --to-destination 172.24.4.3:8888
  243  wget 172.24.4.3:8888
  244  wget 192.168.203.10:54050
  245  sudo passwd
  246  sudo -i
  247  ls
  248  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  249  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  250  sudo iptables -t nat -D PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.24.4.3:8888
  251  sudo iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 192.168.203.10:80
  252  histoyr
  253  history
