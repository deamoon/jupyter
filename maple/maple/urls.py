from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest.models import Spawner
from rest_framework import routers, serializers, viewsets

class SpawnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Spawner
        fields = ("name", "metadata")

class SpawnerViewSet(viewsets.ModelViewSet):
    queryset = Spawner.objects.all()
    serializer_class = SpawnerSerializer

router = routers.DefaultRouter()
router.register(r'spawners', SpawnerViewSet)

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'api/', include(router.urls))
)
