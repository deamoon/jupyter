from django.contrib import admin
from models import PublicIP, DockerImage, Image, Spawner, Flavour, Instance

admin.site.register(PublicIP)
admin.site.register(DockerImage)
admin.site.register(Image)
admin.site.register(Spawner)
admin.site.register(Flavour)
admin.site.register(Instance)
