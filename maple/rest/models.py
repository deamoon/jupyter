from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError

import json

def validate_port(value):
    if value > 65535 or value < 0:
        raise ValidationError("Invalid port {0}, port should be in [0, 65535]".format(value))

class PublicIP(models.Model):
    STATUS_LIST = (
        ("Active", "Active"),
        ("Free", "Free"),
        ("Disabled", "Disabled")
    )

    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Free")
    public_port = models.IntegerField(validators=[validate_port])
    destination_port = models.IntegerField(validators=[validate_port])

    def __unicode__(self):
        return "PublicIP {0} -> {1}, status: {2}".format(self.public_port, self.destination_port, self.status)

class DockerImage(models.Model):
    STATUS_LIST = (
        ("Available", "Available"),
        ("Disabled", "Disabled")
    )

    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Available")
    name = models.CharField(max_length=256, unique=True, default="Unknown image")
    metadata = models.TextField(blank=True, default="{}")
    web_port = models.IntegerField(validators=[validate_port])

    creation_date = models.DateTimeField(auto_now_add=True)
    last_modification_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "DockerImage {0} (web port: {1}, status: {2})".format(self.name, self.web_port, self.status)

class Image(models.Model):
    name = models.CharField(max_length=256, default="Unknown image")
    base_image = models.ForeignKey(DockerImage)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    is_public = models.BooleanField(default=False)

    creation_date = models.DateTimeField(auto_now_add=True)
    last_modification_date = models.DateTimeField(auto_now=True)

    # Any bash script that will be executed on each instance creation
    # (if instance is based on this Image)
    instance_post_creation_action = models.TextField()

    def __unicode__(self):
        return 'Image (owner: {0}, base image: "{1}", public: {2})'.format(
            self.owner.username, self.base_image.name, self.is_public)

class Spawner(models.Model):
    SPAWNER_TYPES = (
        ("os", "OpenStack"),
        ("aws", "Amazon WebServices")
    )

    type = models.CharField(max_length=10, choices=SPAWNER_TYPES, default="os")
    spawner_subclass_metadata = models.TextField(default="{}")

    def __unicode__(self):
        return "Spawner {0}".format(self.name)

class AmazonSpawner(Spawner):
    class Meta:
        proxy = True

    def get_token(self):
        metadata = json.loads(self.spawner_subclass_metadata)
        return metadata["token"]

class Flavour(models.Model):
    spawner = models.ForeignKey(Spawner)

    vcpu_count = models.PositiveIntegerField()
    memory_in_mb = models.PositiveIntegerField()
    hdd_in_mb = models.PositiveIntegerField()

    def __unicode__(self):
        return "Flavour for {0}, VCPU: {1}, RAM: {2} GB, HDD: {2} GB".format(
            self.spawner, self.vcpu_count, self.memory_in_mb / 1024, self.hdd_in_mb / 1024)

class Instance(models.Model):
    STATUS_LIST = (
        ("Running", "Running"),
        ("Stopped", "Stopped"),
        ("Unavailable", "Unavailable")
    )

    name = models.CharField(max_length=256)

    flavour = models.ForeignKey(Flavour)

    image = models.ForeignKey(Image)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    public_url = models.URLField()

    creation_date = models.DateTimeField(auto_now_add=True)
    last_modification_date = models.DateTimeField(auto_now=True)
    available_time = models.TimeField()

    status = models.CharField(max_length=20, choices=STATUS_LIST)

    def __unicode__(self):
        return "Instance {0} (owner: {1}, creation date: {2}, finish_date: {3}, status: {4})".format(
            self.name, self.owner.username, str(self.creation_date), str(self.finish_date), self.status)
