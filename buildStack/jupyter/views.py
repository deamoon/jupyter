from django.shortcuts import render
from instance.models import Instance, Image, PublicIp, DockerImage, SshServer, DockerSshInstance
import json
from registration.users import UserModel
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect
import pytz
import datetime
from django.conf import settings

utc = pytz.UTC

def getServer(flavor):
    """ Search best server for this intance """
    if settings.REMOTE_HOST:
        server = SshServer.objects.get(label="dimacloud")
    else:
        server = SshServer.objects.get(label="internal")
    return server

def isUserHaveMoney(user, context):
    """ Check that user have money for creation """
    if len(Instance.objects.filter(user__id=user.id, status='Active')) > 0:
        context["error_message"] = "Only one server"
        return False
    return True

def checkHealthInstance():
    pass

@login_required
def getJupyter(request):
    context = {}

    meta = {}
    password = request.POST['password']
    if password:
        meta['PASSWORD'] = password

    hours = int(request.POST.get('hours', '8'))
    flavor = request.POST.get('flavor', 'unlimited')
    dockerName = request.POST.get('dockerName', 'jupyter/datascience-notebook')

    InstanceClass = DockerSshInstance

    if not isUserHaveMoney(request.user, context):
        return render(request, 'profile/instance.html', context)

    try:
        freePort = PublicIp.objects.filter(status='Free')[0]
    except:
        context["error_message"] = "No public urls"
        return render(request, 'profile/instance.html', context)

    server = getServer(flavor)
    if not server:
        context["error_message"] = 'Server error'
        return render(request, 'profile/instance.html', context)

    baseImage = DockerImage.objects.get(name=dockerName)
    image = Image(name="test", meta=json.dumps(meta), baseImage=baseImage, user=request.user)
    image.save()

    instance = InstanceClass(flavor=flavor, image=image, user=request.user, server=server, publicIp=freePort)

    if hours > 0:
        instance.endDate = utc.localize(datetime.datetime.now()) + datetime.timedelta(hours=hours)
    instance.save()

    if instance.status == 'Active':
        context['publicUrl'] = instance.url
    else:
        context['error_message'] = 'Server error'
        return render(request, 'profile/instance.html', context)

    return render(request, 'profile/instance.html', context)

@login_required
def get_jupyter_old(request):
    meta = {
        "PASSWORD": request.POST['password'],
    }
    context = {
        'publicUrl': "",
        'error_message': "",
    }

    if len(Instance.objects.filter(user__id=request.user.id)) > 0:
        context["error_message"] = "Only one server"

    try:
        free_port = PublicIp.objects.filter(status='Free')[0]
    except:
        context["error_message"] = "No public urls"

    if not context["error_message"]:
        base_image = DockerImage.objects.get(name="jupyter/datascience-notebook")
        image = Image(name="test", meta=json.dumps(meta), base_image=base_image, user=request.user)
        image.save()
        # try:
        instance = Instance(name="test", flavor="m1.small", public_ip=free_port, image=image, user=request.user)
        instance.save()
        context["publicUrl"] = free_port.public_url
        # except:
        #     context["error_message"] = "Server error"

    return render(request, 'profile/instance.html', context)

@login_required
def get_all_instances(request):
    context = {
        'instances': [inst.as_child() for inst in Instance.objects.filter(user__id=request.user.id)],
        'ip_free': len(PublicIp.objects.filter(status='Free')),
        'active_page': 'index',
    }
    return render(request, 'profile/index.html', context)

@login_required
def delete_instance(request, instanceId):
    instance = get_object_or_404(Instance, pk=instanceId)
    if request.user.id == instance.user.id:
        instance.as_child().delete()
    return redirect("profile_main")

def jupyter_main(request):
    if request.user.is_authenticated():
        return redirect("profile_main")
    else:
        context = {
            'ip_free': len(PublicIp.objects.filter(status='Free')),
        }
        context['ip_all'] = context['ip_free'] + len(PublicIp.objects.filter(status='Active'))
        return render(request, 'index.html', context)

def faq(request):
    context = {
            'ip_free': len(PublicIp.objects.filter(status='Free')),
        }
    context['ip_all'] = context['ip_free'] + len(PublicIp.objects.filter(status='Active'))
    return render(request, 'faq.html', context)

def sandbox(request):
    context = {
        'active_page': 'sandbox',
    }
    return render(request, 'profile/sandbox.html', context)

def flat(request):
    context = {
            'ip_free': len(PublicIp.objects.filter(status='Free')),
        }
    context['ip_all'] = context['ip_free'] + len(PublicIp.objects.filter(status='Active'))
    return render(request, 'flat-ui.html', context)

def landing(request):
    if request.user.is_authenticated():
        return redirect("profile_main")
    context = {
            'ip_free': len(PublicIp.objects.filter(status='Free')),
        }
    context['ip_all'] = context['ip_free'] + len(PublicIp.objects.filter(status='Active'))
    return render(request, 'landing/index.html', context)