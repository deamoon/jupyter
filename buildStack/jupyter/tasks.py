from celery import task
from instance.models import Instance
import pytz
import datetime

utc = pytz.UTC

@task()
def control_jupiter_notebook():
    for instance in Instance.objects.all():
        if utc.localize(datetime.datetime.now()) > instance.end_date:
            instance.delete()