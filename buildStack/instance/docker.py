import sys
import docker

def findDockerId(novaId):
    client = docker.Client(base_url='unix://var/run/docker.sock', version='auto')
    for container in client.containers():
        for name in container['Names']:
            if novaId in name:
                return container['Id']
    return None

def runCmdInDocker(novaId, cmd):
    dockerId = findDockerId(novaId)
    client = docker.Client(base_url='unix://var/run/docker.sock', version='auto')
    return client.exec_start(client.exec_create(container=dockerId, cmd=cmd)['Id'])

def main():
    print runCmdInDocker(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    main()