from instance.models import Instance, Image, PublicIp, DockerImage, SshServer

def get_parsed_route_list(string):
    def _print_res(main_obj):
        for obj in main_obj:
            if isinstance(obj, dict) or isinstance(obj, list):
                _print_res(obj)
            print(obj)
        pass

    result_mass = string.split("Chain ")[1::]
    result = {}
    for part in result_mass:
        part = part.strip()
        line_count = 0
        chain_model = {}
        headers = []
        models = []
        for line in part.splitlines():
            if line_count is 0:
                result[line.split(" ")[0]] = {'models': models}
            elif line_count is 1:  # headers
                headers = line.split()
            else:
                model_obj = {}
                not_parsed_obj = line.split()
                for i in range(len(headers)):
                    model_obj[headers[i]] = not_parsed_obj[i]
                    pass

                if len(headers) < len(not_parsed_obj) and not (model_obj['target'] in 'DOCKER'):
                    add_array = not_parsed_obj[len(headers)::]
                    additional_info = {
                        'address_type': add_array[0],
                        'dist_port': add_array[1].split(':')[1],
                        'to_address': add_array[2].split(':')[1],
                        'to_port': add_array[2].split(':')[2]
                    }
                    model_obj['additional-info'] = additional_info
                    pass
                models.append(model_obj)
                pass

            line_count += 1
            pass
        if len(models) > 0:
            chain_model["sub_nodes"] = models
            pass
        pass
    return result

def initDatabase():
    DockerImage(status='Up',
                name='jupyter/datascience-notebook',
                meta='{"PASSWORD":"admin"}',
                webPort=8888,
                dockerId='4083b2a5-c9e9-428d-bed3-3cad99dcb928').save()

    SshServer(login='ubuntu',
              port=54161,
              privateKeyName='dimacloud.key',
              host='login.miptcloud.com',
              internalIp='10.77.64.161',
              label='dimacloud').save()

    SshServer(login='ubuntu',
              privateKeyName='dimacloud.key',
              host='10.77.64.161',
              internalIp='10.77.64.161',
              label='internal').save()

    for num in range(10, 31):
        PublicIp(status='Free',
                 publicUrl='http://570{0}.jupyter.ml'.format(num),
                 iptablePort='540{0}'.format(num)).save()

def main():
    string = """Chain PREROUTING (policy ACCEPT 2 packets, 120 bytes)
 pkts bytes target     prot opt in     out     source               destination
 272K   16M neutron-openvswi-PREROUTING  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 272K   16M nova-api-PREROUTING  all  --  *      *       0.0.0.0/0            0.0.0.0/0
14229  950K DOCKER     all  --  *      *       0.0.0.0/0            0.0.0.0/0            ADDRTYPE match dst-type LOCAL
 1076 57617 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54020 to:172.24.4.51:8888
  196 11778 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54019 to:172.24.4.52:8888
    4   240 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54010 to:172.24.4.57:8888
 2760  144K DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54011 to:172.24.4.58:8888
   53  2716 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54013 to:172.24.4.76:8888
   21  1260 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54015 to:172.24.4.94:8888
  187 10636 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54050 to:10.77.64.161:80
    0     0 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54024 to:172.24.4.140:8888
    0     0 DNAT       tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:54025 to:172.24.4.141:8888

Chain INPUT (policy ACCEPT 2 packets, 120 bytes)
 pkts bytes target     prot opt in     out     source               destination

Chain OUTPUT (policy ACCEPT 11 packets, 652 bytes)
 pkts bytes target     prot opt in     out     source               destination
 380K   23M neutron-openvswi-OUTPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 380K   23M nova-api-OUTPUT  all  --  *      *       0.0.0.0/0            0.0.0.0/0
 340K   20M DOCKER     all  --  *      *       0.0.0.0/0           !127.0.0.0/8          ADDRTYPE match dst-type LOCAL
"""
    # objc = get_parsed_route_list(string)
    # from pprint import pprint
    # pprint(objc['PREROUTING']['models'])
    initDatabase()

if __name__ == '__main__':
	main()
