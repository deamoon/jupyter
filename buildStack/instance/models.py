from django.db import models
import ssh
from django.conf import settings
import json
import pytz
import datetime
import os
import paramiko

utc = pytz.UTC

class PublicIp(models.Model):
    """ Floating IP """
    STATUS_LIST = (
        ('Active', 'Active'),
        ('Free', 'Free'),
        ('Delete', 'Delete'),
        ('Error', 'Error'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Error")
    publicUrl = models.CharField(max_length=100)
    iptablePort = models.CharField(max_length=20)
    def __unicode__(self):
        return "PublicIp " + " ".join([self.status, self.publicUrl, self.iptablePort])

class DockerImage(models.Model):
    """
    Anything docker container, which can be docker pull
    DockerImages = {
        "notebook" : ("ipython/notebook", "8888", {"USE_HTTP":"1", "PASSWORD":kwargs.get("password", "admin")}),
        "wordpress" : ("tutum/wordpress", "80", {})
    }
    """
    STATUS_LIST = (
        ('Up', 'Up'),  # image was installed on server
        ('Error', 'Error'),
        ('Delete', 'Delete'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Error")
    name = models.CharField(max_length=200, unique=True) # tutum/wordpress
    meta = models.TextField(default="{}", blank=True)
    webPort = models.CharField(max_length=20)

    dateCreation = models.DateTimeField(auto_now_add=True)
    dateLastModification = models.DateTimeField(auto_now=True)

    dockerId = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if self.meta:
            dict(json.loads(self.meta))  # check format meta
        else:
            self.meta = '{}'

        create = kwargs.get('create', True)
        if create and self.status != 'Up':
            self.__create()

        super(DockerImage, self).save(*args, **kwargs)

    def __create(self):
        try:
            _, res_save = ssh.docker_create(self)
            self.dockerId = res_save['id']
            self.status = 'Up'
            return res_save
        except:
            self.status = 'Error'
            raise

    def delete_nova(self):
        try:
            res_create = ssh.docker_delete(self)
            self.status = 'Delete'
            return res_create
        except:
            self.status = 'Error'
            raise

    def __unicode__(self):
        return "DockerImage " + " ".join([self.status, self.name])

class Image(models.Model):
    """
    Users docker container with one open webPort and meta information
    Example:
        meta = {"PASSWORD":admin}
        docker run -e PASSWORD=admin
    """
    name = models.CharField(max_length=200, default="unknown_image") # wordpress

    baseImage = models.ForeignKey(DockerImage)

    meta = models.TextField(default="{}", blank=True)
    postCreationScript = models.TextField(default="", blank=True)

    dateCreation = models.DateTimeField(auto_now_add=True)
    dateLastModification = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def save(self, *args, **kwargs):
        if not self.meta:
            self.meta = '{}'
        parent_dict = dict(json.loads(self.baseImage.meta))
        my_dict = dict(json.loads(self.meta))
        parent_dict.update(my_dict)
        self.meta = json.dumps(parent_dict)
        super(Image, self).save(*args, **kwargs)

    def __unicode__(self):
        return "Image " + " ".join([self.name, str(self.baseImage), self.user.username])

class KnowsChild(models.Model):
    # Make a place to store the class name of the child
    _my_subclass = models.CharField(max_length=200)

    class Meta:
        abstract = True

    def as_child(self):
        return getattr(self, self._my_subclass)

    def save(self, *args, **kwargs):
        # save what kind we are.
        self._my_subclass = self.__class__.__name__.lower()
        super(KnowsChild, self).save(*args, **kwargs)

class Server(KnowsChild):
    """ Our resources """
    label = models.CharField(max_length=50, unique=True)

class Instance(KnowsChild):
    """ Abstract instance """
    STATUS_LIST = (
        ('Active', 'Active'), # image was installed on server
        ('Error', 'Error'),
    )
    status = models.CharField(max_length=20, choices=STATUS_LIST, default="Error")

    FLAVOR_LIST = (
        ('tiny', 'tiny, 1 VCPU, 512 MB, 1 GB'),
        ('small', 'small, 1 VCPU, 2048 MB, 20 GB'),
        ('medium', 'medium, 2 VCPU, 4096 MB, 40 GB'),
        ('large', 'large, 4 VCPU, 8192 MB, 80 GB'),
        ('xlarge', 'xlarge, 8 VCPU, 16384 MB, 160 GB'),
        ('unlimited', 'without specifying quotas'),
    )

    image = models.ForeignKey(Image)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    flavor = models.CharField(max_length=20, choices=FLAVOR_LIST, default="tiny")
    url = models.CharField(max_length=256, blank=True)
    endDate = models.DateTimeField()

    logs = models.CharField(max_length=256, default='', blank=True)
    dateCreation = models.DateTimeField(auto_now_add=True)
    dateLastModification = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "Instance " + " ".join([str(self.image), str(self.user)])

class SshServer(Server):
    login = models.CharField(max_length=50, default='ubuntu')
    host = models.CharField(max_length=256, default="login.miptcloud.com")
    port = models.IntegerField(default=22)
    password = models.CharField(max_length=50, default="", blank=True) # don't support autorization with password
    privateKeyName = models.CharField(max_length=256, default="dimacloud.key", blank=True)
    internalIp = models.GenericIPAddressField(blank=True, null=True)

    def execCommand(self, command):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=self.host,
                       username=self.login,
                       port=self.port,
                       key_filename=os.path.join(settings.SSH_KEY_FOLDER, self.privateKeyName))
        stdin, stdout, stderr = client.exec_command(command)
        stdout, stderr = stdout.read(), stderr.read()
        client.close()
        return stdout, stderr

class DockerSshInstance(Instance):
    """ Docker Instance """
    publicIp = models.ForeignKey(PublicIp)
    container = models.CharField(max_length=256)
    server = models.ForeignKey(SshServer)

    def _run(self):
        meta = json.loads(self.image.meta)
        meta_str = ' '.join(["-e {0}={1}".format(key, value) for key, value in meta.items()])
        command = 'docker run -d -p {iptablePort}:{webPort} {meta_str} {docker_name}'.format(
                    iptablePort=self.publicIp.iptablePort,
                    webPort=self.image.baseImage.webPort,
                    meta_str=meta_str,
                    docker_name=self.image.baseImage.name)
        out, err = self.server.execCommand(command)
        self.container = out.strip()
        return err

    def _forward(self, action):
        """ action: Add, Delete """
        command = 'sudo iptables -t nat -{action} PREROUTING -i eth0 -p tcp -m tcp \
                  --dport {iptablePort} -j DNAT --to-destination \
                  {internalIp}:{iptablePort}'.format(internalIp=self.server.internalIp,
                                                      iptablePort=self.publicIp.iptablePort,
                                                      action=action[0])
        out, err = self.server.execCommand(command)
        return err

    def _delete(self):
        command = 'docker rm -f {container}'.format(container=self.container)
        out, err = self.server.execCommand(command)
        return err

    def save(self, *args, **kwargs):
        errorRun = self._run()

        if errorRun:
            self.status = 'Error'
            self.logs += '\n' + errorRun
        else:
            errorForward = self._forward('Add')
            if errorForward:
                self.publicIp.status = 'Error'
                self.publicIp.save()
                self.status = 'Error'
                self.logs += '\n' + errorForward

                self.logs += '\n' + self._delete()
            else:
                # self.checkHealth() TODO
                self.status = 'Active'
                self.url = self.publicIp.publicUrl

        super(DockerSshInstance, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        errorDelete = self._delete()
        if errorDelete:
            self.status = 'Error'
            self.logs += '\n' + errorDelete
            self.save()
        else:
            errorForward = self._forward('Delete')
            if errorForward:
                self.publicIp.status = 'Error'
            else:
                self.publicIp.status = 'Free'
            self.publicIp.save()
            super(DockerSshInstance, self).delete(*args, **kwargs)

    def __unicode__(self):
        return "DockerSshInstance " + " ".join([str(self.image), self.flavor, str(self.endDate), str(self.user)])

# class AmazonServer(Server):
#     pass

# class AzureServer(Server):
#     pass

# class DigitalOceanServer(Server):
#     pass

# class OpenStackInstance(Instance):
#     """ OpenStack doesn't support now """
#     network_id = "6e5c81ec-a993-4c0e-8a3f-5d9a43b5dd80" # internal
#     floating_ip = models.CharField(max_length=50, default="unknown_floating_ip")
#     name = models.CharField(max_length=200, default="unknown_name")
#     instance_id = models.CharField(max_length=200, default="unknown_instance_id")
#     publicIp = models.ForeignKey(PublicIp)

#     def save(self, *args, **kwargs):
#         create = kwargs.get('create', True)
#         activate = kwargs.get('activate', True)
#         if create and self.status not in ('Active', 'Up'):
#             self.__create()
#             self.endDate = utc.localize(datetime.datetime.now()) + datetime.timedelta(hours=8)
#         if activate and self.status != 'Active':
#             self.__activate()
#         super(OpenStackInstance, self).save(*args, **kwargs)

#     def delete(self, *args, **kwargs):
#         delete = kwargs.get('delete', True)
#         deactivate = kwargs.get('deactivate', True)
#         if delete:
#             self.__deactivate()
#         if deactivate:
#             self.__delete_instance()
#         super(OpenStackInstance, self).delete(*args, **kwargs)

#     def __delete_instance(self):
#         ssh.nova_delete(self)

#     def __create(self, **kwargs):
#         try:
#             instance_dict = ssh.create_instance(self)
#             self.status = "Up"

#             self.instance_id = instance_dict.get("id", "unknown_instance_id")
#             self.floating_ip = instance_dict.get("ip",  "unknown_ip")
#             return instance_dict
#         except:
#             self.status = "Error"
#             raise

#     def __deactivate(self):
#         try:
#             print(self.publicIp.status)
#             if self.publicIp and self.publicIp.status == 'Active':
#                 ssh.deactivate(self.floating_ip, self.image.baseImage.webPort, self.publicIp.iptablePort)
#             self.publicIp.status = 'Free'
#         except:
#             self.publicIp.status = 'Error'
#             raise
#         finally:
#             self.publicIp.save()

#     def __activate(self):
#         try:
#             res_deactivate = self.__deactivate()
#             res_activate = ssh.activate(self.floating_ip, self.image.baseImage.webPort, self.publicIp.iptablePort)
#             self.status = "Active"
#             self.publicIp.status = 'Active'
#             return str(res_deactivate) + str(res_activate)
#         except:
#             self.status = "Error"
#             raise
#         finally:
#             self.publicIp.save()