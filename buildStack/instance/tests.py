from django.test import SimpleTestCase
from django.contrib.auth.models import User
import time
import requests

from .models import *

class InstanceRunTest(SimpleTestCase):
    
    def setUp(self):
        self.instances = []

    def __check_availiable(self, url, password=''):
        url_login = '{0}/login?next=%2Ftree'.format(url)
        params = {'password':''}
        post_response = requests.post(url_login, data=params)
        self.assertEqual(post_response.status_code, 200)
        get_resp = requests.get('{0}/tree'.format(url), cookies=post_response.history[0].cookies)
        self.assertEqual(get_resp.status_code, 200)
        
    def __run_instance(self):
        meta = {
            "PASSWORD": "",
        }

        free_port = PublicIp.objects.filter(status='Free')[0]
        base_image = DockerImage.objects.get(name="ipython/scipyserver")
        test_user = User.objects.get(username='rush')
        image = Image(name="test", meta=json.dumps(meta), base_image=base_image, user=test_user)
        image.save()
        
        instance = Instance(name="test", flavor="m1.small", public_ip=free_port, image=image, user=test_user)
        instance.save()
        
        self.instances.append(instance)
        return instance


    def __test_instance(self):
        print 'Running instance...'
        instance = self.__run_instance()
        url = instance.public_ip.public_url
        time.sleep(2)
        self.__check_availiable(url)


    def runTest(self):
        for i in range(5):
            self.__test_instance()


    def tearDown(self):
        for inst in self.instances:
            inst.delete()

