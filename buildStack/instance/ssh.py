# ssh ubuntu@login.miptcloud.com -p 54030 -i miptcloud/cloud.key -o "StrictHostKeyChecking no"
# ssh ubuntu@10.77.64.120 -i cloud-1c.key -p 1907

import paramiko
import re
import json
from django.conf import settings
import os
import subprocess
from docker import runCmdInDocker
import sys

def parse_result(data):
    parse = data.split("\n")
    parse2 = [i.split("|") for i in parse if len(i.split("|")) > 2]
    dic = {}
    for l in parse2:
        items = [i.strip() for i in l if i.strip()]
        if len(items) > 1:
            dic[items[0]] = items[1]
    return dic

def exec_command_remote(command, prefix=""):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname='login.miptcloud.com',
                   username='ubuntu',
                   port=54161,
                   key_filename='../deploy/dimacloud.key',)
    stdin, stdout, stderr = client.exec_command(prefix + command)
    data = stdout.read() + stderr.read()
    client.close()
    return data

def exec_command(command):
    if settings.REMOTE_HOST:
        prefix = ". /home/ubuntu/devstack/openrc admin; "
        return exec_command_remote(command, prefix)
    else:
        return subprocess.check_output(command, shell=True)

def iptable_command(active, host, port, iptable_port):
    return "sudo iptables -t nat -{0} PREROUTING -i eth0 -p tcp -m tcp\
            --dport {3} -j DNAT --to-destination {1}:{2}".format(active, host, port, iptable_port)

def activate(host, port, iptable_port):
    command = iptable_command('A', host, port, iptable_port)
    result = exec_command(command)
    # check(result) TODO
    return result

def deactivate(host, port, iptable_port):
    command = iptable_command('D', host, port, iptable_port)
    result = exec_command(command)
    # check(result) TODO
    return result

def docker_pull(base_image):
    command = "docker pull {0}".format(base_image.name)
    result = exec_command(command)
    # check(result) TODO
    return result

def docker_save(base_image):
    command = "docker save {0} | glance image-create --is-public=True\
               --container-format=docker --disk-format=raw --name {0}".format(base_image.name)
    result = exec_command(command)
    # check(result) TODO
    return parse_result(result)

def docker_delete(base_image):
    command = "nova image-delete {0}".format(base_image.name)
    result = exec_command(command)
    # check(result) TODO
    return result

def docker_create(base_image):
    res_pull = docker_pull(base_image)
    res_save = docker_save(base_image)
    return res_pull, res_save

def nova_boot(instance):
    """
    +--------------------------------------+---------------------------------------------------------+
    | Property                             | Value                                                   |
    +--------------------------------------+---------------------------------------------------------+
    | OS-DCF:diskConfig                    | MANUAL                                                  |
    | OS-EXT-AZ:availability_zone          | nova                                                    |
    ...
    """
    image = instance.image
    meta = json.loads(image.meta)
    meta_str = ' '.join(["--meta ENV_{0}={1}".format(key, value) for key, value in meta.items()])
    command = "nova boot --image {image} --flavor {flavor} \
               {meta} --nic net-id={network} {name}".format(image=image.base_image.docker_id,
                                                     flavor="m1." + instance.flavor,
                                                     meta=meta_str,
                                                     name=instance.name,
                                                     network=instance.network_id)
    data = exec_command(command)
    return parse_result(data)

def nova_delete(instance):
    command = "nova delete {0}".format(instance.instance_id)
    data = exec_command(command)
    return data

def nova_floating_create():
    """
    +------------+-----------+----------+--------+
    | Ip         | Server Id | Fixed Ip | Pool   |
    +------------+-----------+----------+--------+
    | 172.24.4.9 | -         | -        | public |
    +------------+-----------+----------+--------+
    """
    command = "nova floating-ip-create public"
    data = exec_command(command)
    return re.findall(r'[0-9]+(?:\.[0-9]+){3}', data)[0]

def nova_floating_add(id_instance, ip):
    command = "nova add-floating-ip {id_instance} {ip}".format(id_instance=id_instance, ip=ip)
    return exec_command(command)

def runCommandInDocker(novaId, cmd):
    command = "python instance/docker.py {novaId} {cmd}".format(novaId=novaId, cmd=cmd)
    return exec_command(command)

def create_instance(instance):
    instance_dict = nova_boot(instance)
    instance_dict["ip"] = nova_floating_create()
    nova_floating_add(instance_dict["id"], instance_dict["ip"])
    runCmdInDocker(instance_dict['id'], instance.image.post_creation_script)
    return instance_dict

def nova_list():
    command = "nova list"
    return exec_command(command)