from django.contrib import admin
from models import Instance, Image, DockerImage, PublicIp, Server, SshServer, DockerSshInstance

admin.site.register(SshServer)
admin.site.register(Instance)
admin.site.register(DockerSshInstance)
admin.site.register(Image)
admin.site.register(DockerImage)
admin.site.register(PublicIp)