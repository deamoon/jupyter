# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DockerImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'Error', max_length=20, choices=[(b'Up', b'Up'), (b'Error', b'Error'), (b'Delete', b'Delete')])),
                ('name', models.CharField(unique=True, max_length=200)),
                ('meta', models.TextField(default=b'{}', blank=True)),
                ('webPort', models.CharField(max_length=20)),
                ('dateCreation', models.DateTimeField(auto_now_add=True)),
                ('dateLastModification', models.DateTimeField(auto_now=True)),
                ('dockerId', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'unknown_image', max_length=200)),
                ('meta', models.TextField(default=b'{}', blank=True)),
                ('postCreationScript', models.TextField(default=b'', blank=True)),
                ('dateCreation', models.DateTimeField(auto_now_add=True)),
                ('dateLastModification', models.DateTimeField(auto_now=True)),
                ('baseImage', models.ForeignKey(to='instance.DockerImage')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Instance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_my_subclass', models.CharField(max_length=200)),
                ('status', models.CharField(default=b'Error', max_length=20, choices=[(b'Active', b'Active'), (b'Error', b'Error')])),
                ('flavor', models.CharField(default=b'tiny', max_length=20, choices=[(b'tiny', b'tiny, 1 VCPU, 512 MB, 1 GB'), (b'small', b'small, 1 VCPU, 2048 MB, 20 GB'), (b'medium', b'medium, 2 VCPU, 4096 MB, 40 GB'), (b'large', b'large, 4 VCPU, 8192 MB, 80 GB'), (b'xlarge', b'xlarge, 8 VCPU, 16384 MB, 160 GB'), (b'unlimited', b'without specifying quotas')])),
                ('url', models.CharField(max_length=256, blank=True)),
                ('endDate', models.DateTimeField()),
                ('logs', models.CharField(default=b'', max_length=256, blank=True)),
                ('dateCreation', models.DateTimeField(auto_now_add=True)),
                ('dateLastModification', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PublicIp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'Error', max_length=20, choices=[(b'Active', b'Active'), (b'Free', b'Free'), (b'Delete', b'Delete'), (b'Error', b'Error')])),
                ('publicUrl', models.CharField(max_length=100)),
                ('iptablePort', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_my_subclass', models.CharField(max_length=200)),
                ('label', models.CharField(unique=True, max_length=50)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DockerSshInstance',
            fields=[
                ('instance_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='instance.Instance')),
                ('container', models.CharField(max_length=256)),
                ('publicIp', models.ForeignKey(to='instance.PublicIp')),
            ],
            options={
                'abstract': False,
            },
            bases=('instance.instance',),
        ),
        migrations.CreateModel(
            name='SshServer',
            fields=[
                ('server_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='instance.Server')),
                ('login', models.CharField(default=b'ubuntu', max_length=50)),
                ('host', models.CharField(default=b'login.miptcloud.com', max_length=256)),
                ('port', models.IntegerField(default=22)),
                ('password', models.CharField(default=b'', max_length=50, blank=True)),
                ('privateKeyName', models.CharField(default=b'dimacloud.key', max_length=256, blank=True)),
                ('internalIp', models.GenericIPAddressField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('instance.server',),
        ),
        migrations.AddField(
            model_name='instance',
            name='image',
            field=models.ForeignKey(to='instance.Image'),
        ),
        migrations.AddField(
            model_name='instance',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='dockersshinstance',
            name='server',
            field=models.ForeignKey(to='instance.SshServer'),
        ),
    ]
