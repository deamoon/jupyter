from instance.models import PublicIp

def init_public_ips(ip_range):
    for ip in ip_range:
        public_ip = 'http://login.miptcloud.com:570{0}'.format(ip)
        ip_table_port = '540{0}'.format(ip)
        new_ip = PublicIp(status='Free', public_url=public_ip, iptable_port=ip_table_port)
        new_ip.save()


from django.test.runner import DiscoverRunner

class NoDbTestRunner(DiscoverRunner):
  """ A test runner to test without database creation """

  def setup_databases(self, **kwargs):
    """ Override the database creation defined in parent class """
    pass

  def teardown_databases(self, old_config, **kwargs):
    """ Override the database teardown defined in parent class """
    pass