data = """+--------------------------------------+---------------------------------------------------------+
| Property                             | Value                                                   |
+--------------------------------------+---------------------------------------------------------+
| OS-DCF:diskConfig                    | MANUAL                                                  |
| OS-EXT-AZ:availability_zone          | nova                                                    |
| OS-EXT-SRV-ATTR:host                 | -                                                       |
| OS-EXT-SRV-ATTR:hypervisor_hostname  | -                                                       |
| OS-EXT-SRV-ATTR:instance_name        | instance-00000008                                       |
| OS-EXT-STS:power_state               | 0                                                       |
| OS-EXT-STS:task_state                | scheduling                                              |
| OS-EXT-STS:vm_state                  | building                                                |
| OS-SRV-USG:launched_at               | -                                                       |
| OS-SRV-USG:terminated_at             | -                                                       |
| accessIPv4                           |                                                         |
| accessIPv6                           |                                                         |
| adminPass                            | n668AfZYZoTX                                            |
| config_drive                         |                                                         |
| created                              | 2015-05-03T19:43:29Z                                    |
| flavor                               | m1.tiny (1)                                             |
| hostId                               |                                                         |
| id                                   | b0585b1a-fb29-470f-9ba3-4097672e7716                    |
| image                                | ipython/notebook (2ac0661a-4023-45ba-83da-28231a683225) |
| key_name                             | -                                                       |
| metadata                             | {"ENV_USE_HTTP": "1", "ENV_PASSWORD": "admin"}          |
| name                                 | my_notebook                                             |
| os-extended-volumes:volumes_attached | []                                                      |
| progress                             | 0                                                       |
| security_groups                      | default                                                 |
| status                               | BUILD                                                   |
| tenant_id                            | da93c819f4374f7997242920152a7c54                        |
| updated                              | 2015-05-03T19:43:29Z                                    |
| user_id                              | 396ae2cda118440aa8b01742a304b644                        |
+--------------------------------------+---------------------------------------------------------+
"""

parse = data.split("\n")
parse2 = [i.split("|") for i in parse if len(i.split("|")) > 2]
dic = {}
for l in parse2:
	items = [i.strip() for i in l if i.strip()]
	if len(items) > 1:
		dic[items[0]] = items[1]

# parse3 = [i.strip() for l in parse2 for i in l]
# print dic

import re

data="""+------------+-----------+----------+--------+
| Ip         | Server Id | Fixed Ip | Pool   |
+------------+-----------+----------+--------+
| 172.24.4.9 | -         | -        | public |
+------------+-----------+----------+--------+
"""

print re.findall(r'[0-9]+(?:\.[0-9]+){3}', data)

# parse = data.split("\n")
# parse2 = [i.split("|") for i in parse if len(i.split("|")) > 2]
# dic = {}
# for l in parse2:
# 	items = [i.strip() for i in l if i.strip()]
# 	if len(items) > 3:
# 		dic[items[0]] = items[-1]

# print dic

