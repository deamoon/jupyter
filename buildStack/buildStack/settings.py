"""
Django settings for buildStack project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# templates
SETTINGS_DIR = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(SETTINGS_DIR, os.pardir)
PROJECT_ROOT = os.path.abspath(PROJECT_PATH)

REMOTE_HOST = False # True if buildStack on remote server
SSH_KEY_FOLDER = os.path.join(PROJECT_ROOT, 'ssh_keys')

SECRET_KEY = '21dxqnc)x#obsdc0s-9^0g^2a#acnrv6_a%38^59&t#6c1_f60'

DEBUG = True

TEMPLATE_DEBUG = True

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'instance',
    'rest_framework',
    'registration',
    'djcelery',
    'jupyter',
    'djkombu',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'buildStack.urls'

WSGI_APPLICATION = 'buildStack.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
LANGUAGE_CODE = 'en-EN'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# django-registration-redux
ACCOUNT_ACTIVATION_DAYS = 7 # One-week activation window; you may, of course, use a different value.
REGISTRATION_AUTO_LOGIN = True # Automatically log the user in.

# django.contrib.sites
SITE_ID = 2
ALLOWED_HOSTS = ['*']

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)

# email
AUTH_USER_EMAIL_UNIQUE = True
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'support@quantforces.com'
EMAIL_HOST_PASSWORD = 'diman95quant'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'support@quantforces.com'


#This did the trick
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# static
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# RestAPI
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

# Celery
CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'

from datetime import timedelta

CELERYBEAT_SCHEDULE = {
    'control_jupiter_notebook': {
        'task': 'jupyter.tasks.control_jupiter_notebook',
        'schedule': timedelta(minutes=5),
        'args': (),
    },
}

CELERY_TIMEZONE = 'UTC'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
BROKER_URL = 'django://'

try:
    from local_settings import *
except ImportError:
    pass